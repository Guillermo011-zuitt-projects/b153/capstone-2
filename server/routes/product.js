const express = require("express")
const router = express.Router()
const productController = require("../controllers/product");
const auth = require("../auth")

router.post("/", auth.verify, (req, res) => {
if (auth.decode(req.headers.authorization).isAdmin){
productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
}else{
	return res.send(false)
}
})

router.get("/", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))

})

router.get("/:productId", (req, res) => {
	productController.getSpecificProducts(req.params.productId).then(resultFromController => res.send(resultFromController))
})

router.put("/:productId", auth.verify, (req, res) => {
if (auth.decode(req.headers.authorization).isAdmin){
	productController.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController))
}else{
	return res.send(false)
}
})

router.delete("/:productId", auth.verify, (req, res) => {
if (auth.decode(req.headers.authorization).isAdmin){
	productController.archiveProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
}else{
	return res.send(false)
}
})

module.exports = router

