const express = require("express")
const router = express.Router()
const userController = require("../controllers/user")
const auth = require("../auth")

router.post("/register", (req, res) => {
	userController.register(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.login(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/profile", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id
	userController.userProfile(userId).then(resultFromController => res.send(resultFromController))
})

router.get("/myorders", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id
	userController.userOrders(userId).then(resultFromController => res.send(resultFromController))
})

router.post("/checkout", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id
	userController.userCheckout(userId, req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/orders", auth.verify, (req, res) => {
if (auth.decode(req.headers.authorization).isAdmin){
	userController.allUserOrders().then(resultFromController => res.send(resultFromController))
}else{
	return res.send(false)
}
})



module.exports = router;