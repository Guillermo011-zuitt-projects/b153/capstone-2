const User = require("../models/user")
const bcrypt = require("bcrypt")
const auth = require("../auth")


module.exports.register = (body) => {
	let newUser = new User({
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		password: bcrypt.hashSync(body.password, 10),
		mobileNo: body.mobileNo
	})

return newUser.save().then((user, error) => {
		if (error){
		return false;
	}else{
		return true;
	}
})
}

module.exports.login = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return false
			res.send("No Email found")
		}else {
		const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)
		if (isPasswordCorrect){
			return {accessToken: auth.createAccessToken(result.toObject())}
		}else{
			return false
			res.send("Password Incorrect")
		}
		}

	})
}

module.exports.userProfile = (userId) => {
	return User.findById(userId).then(result => {
		result.password = undefined 
		result.orders = undefined 
		return result
	})
}

module.exports.userOrders = (userId) => {
	return User.findById(userId).then(result => {
		result.password = undefined 
		result.isAdmin = undefined
		result.firstName = undefined
		result.lastName = undefined
		result.mobileNo = undefined
		result.email = undefined
		return result
	})
}

module.exports.userCheckout = (userId, body) => {
		return User.findById(userId).then(user => {
		user.orders.push({
			products: {productId: body.productId, quantity: body.quantity},
			totalAmount: body.totalAmount
		})
		return user.save().then((user, error) => {
			if (error) {
				return false;
			}else{
				return true
			}
		})
	})
}

module.exports.allUserOrders = () => {
	return User.find().then(result => {
		return result
	})
}