const Product = require("../models/product")

module.exports.addProduct = (body) => {
	let newProduct = new Product({
		productName: body.productName,
		description: body.description,
		price: body.price
	})
return newProduct.save().then((product, error) => {
	if (error){
		return false;
	}else{
		return true
	}
})
}

module.exports.getActiveProducts = () =>{
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getSpecificProducts = (productId) =>{
	return Product.findById(productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (productId, body) => {
	let updatedProduct = {
		productName: body.productName,
		description: body.description,
		price: body.price
	}
	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.archiveProduct = (productId) => {
	let updatedProduct = {
			isActive: false
	}
	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

