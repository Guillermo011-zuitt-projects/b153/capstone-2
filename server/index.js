const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")


mongoose.connect("mongodb+srv://admin:admin@cluster0.rmv5r.mongodb.net/capstone-2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})


mongoose.connection.once('open', () => console.log("Connected to MongoDB Atlas"))

const app = express()
app.use(express.json())
app.use(express.urlencoded({
	extended: true
}))

app.use("/users", userRoutes)
app.use("/products", productRoutes)

const port = 3000

app.listen(process.env.PORT || port, () => {
	console.log(`Server running on port ${port}`)
})